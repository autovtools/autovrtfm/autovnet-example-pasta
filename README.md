# autovnet-example-pasta

An example of using gitlab pages to serve an example `autovnet pasta`.

The purpose is to show approximately what `pasta` will look like when you `serve` it locally.

Most users will want to `serve` `pasta` locally, to leverage on-the-fly customization, powered by `jinja2`.

See the demo site [https://autovtools.gitlab.io/autovrtfm/autovnet-example-pasta/](https://autovtools.gitlab.io/autovrtfm/autovnet-example-pasta/)
for an example of how this looks.

It is rendered from the `examples/pasta` provided with [`autovnet`](https://gitlab.com/autovtools/autovrtfm/autovnet)

## Terminology

* `pasta`
    - a play on [copypasta](https://en.wikipedia.org/wiki/Copypasta)
    - here, `pasta` is generic phrase that refers to text, commands, etc., that is intended to be copied and pasted (e.g in to a terminal)

* `render` / `serve`
    - `pasta` is `rendered` when it is `served` (hosted locally as a static website)
    - docs within a `pasta` directory (`dish`?) are `jinja2` templates, which can contain placeholder variables `{{LIKE_THIS}}`
        * these variables can hold random values, or `player` (`autovnet` user)-specific values
        * so, many `players` can `render` / `serve` the same `pasta dish (?)` and get unique, personalized output

## Templating

`autovnet pasta` uses `jinja` to customize the `pasta` when it is `serve`'d locally.

It replaces placeholders like `{{FOO}}` with values.

Normally, when you `serve` the `pasta` locally, these placeholders will be replaces with values that are correct for your game config, active `APT` profile, etc.


## Security Warning

**DO NOT SERVE UNTRUSTED PASTA**

`pasta` provides **aribtrary code execution** _as a feature_.

  * Why? `pasta` can customize itself to be ready to copy-paste. This requires the ability to run shell commands, scripts, etc.

If a malicious actor provides `pasta` that you later `serve`, they will be able to execute code on your system.
